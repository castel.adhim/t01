package OOP.SIAM.src;



/*KELAS MAHASISWA MEMILIKI ATRIBUT NAMA, NIM, PRODI, DAN TAHUN MASUK*/

public class Mahasiswa {
    private String nama;
    private int nim;
    private String prodi;
    private int tahun;
    private Transkrip[] transkrip;

    public Mahasiswa(String nama, int nim, String prodi, int tahun) {
        this.nama = nama;
        this.nim = nim;
        this.prodi = prodi;
        this.tahun = tahun;
        this.transkrip = new Transkrip[0];
    }

    public String getNama() {
        return nama;
    }

    public int getNim() {
        return nim;
    }

    public String getProdi() {
        return prodi;
    }

    public int getTahun() {
        return tahun;
    }

    public void addTranscript(Transkrip transcript) {
        Transkrip[] newTranscripts = new Transkrip[transkrip.length + 1];
        for (int i = 0; i < transkrip.length; i++) {
            newTranscripts[i] = transkrip[i];
        }
        newTranscripts[transkrip.length] = transcript;
        transkrip = newTranscripts;
    }

    public Transkrip[] getTranskrip() {
        return transkrip;
    }

    public void printTranscripts() {
        System.out.println("Transkrip nilai untuk " + nama + ":");
        for (Transkrip transcript : transkrip) {
            transcript.printInfo();
        }
    }
}




