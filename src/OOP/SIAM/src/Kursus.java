package OOP.SIAM.src;

/*KELAS KURSUS ATAU MATA KULIAH MEMILIKI ATRIBUT NAMA, ID(KODE MATKUL), KREDIT(SKS)*/

public class Kursus {
    private String nama;
    private int id;
    private int kredit;

    public Kursus(String nama, int id, int kredit) {
        this.nama = nama;
        this.id = id;
        this.kredit = kredit;
    }

    public String getNama() {
        return nama;
    }

    public int getId() {
        return id;
    }

    public int getKredit() {
        return kredit;
    }

    public void printInfo() {
        System.out.println("Name: " + nama);
        System.out.println("ID: " + id);
        System.out.println("Credit: " + kredit);
    }
}